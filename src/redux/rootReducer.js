import { combineReducers } from "redux";

import beerReducer from "./beers/reducer";

const rootReducer = combineReducers({
  beer: beerReducer,
});

export default rootReducer;
