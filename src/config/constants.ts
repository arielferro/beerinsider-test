const MESSAGES = {
  BEER_INSIDER: "Beer Insider",
  CATALOG: "Catalog",
  SEARCH: "Search",
  CLOSE: "Close",
  GENERIC_ERROR: "An error has ocurred",
  LOADING: "Loading...",
  NO_BEER_FOUND: "No beer found",
  NO_BEERS_FOUND: "No beers found...",
  SHOWING: "Showing",
  OF: "of",
  beer_details: "Beer Details",
  FILTER_BEERS: "Filter beers",
  ACTIVE_FILTERS: "active filter(s)",
  FILTER_BEERS_BY: "Filter beers by",
  RESET_FILTERS: "Reset filters"
};

export const MODAL_STYLES = {
  
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: "rgba(255, 255, 255, 0.75)",
    },
    content: {
      position: "absolute",
      top: "200px",
      left: "200px",
      right: "200px",
      bottom: "200px",
      border: "1px solid #ccc",
      background: "#000",
      overflow: "auto",
      WebkitOverflowScrolling: "touch",
      borderRadius: "4px",
      outline: "none",
      padding: "20px",
    },
  


}

export default MESSAGES;
