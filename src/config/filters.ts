import { IFilterGroup } from "../interfaces/IFilter";
import IBeer from "../interfaces/IBeer";

const beerFilters: IFilterGroup[] = [
  {
    label: "Beer type",
    multiple: false,
    filters: [
      {
        label: "Light beers",
        isActive: false,
        callbackFn: (beer: IBeer) => beer.abv < 4,
      },
      {
        label: "Medium",
        isActive: false,
        callbackFn: (beer: IBeer) => beer.abv >= 4 && beer.abv <= 8,
      },
      {
        label: "Strong beers",
        isActive: false,
        callbackFn: (beer: IBeer) => beer.abv > 8,
      },
    ],
  },
  {
    label: "Beer blends",
    multiple: true,
    filters: [
      {
        label: "Lager",
        isActive: false,
        callbackFn: (beer: IBeer) =>
          (beer.description + beer.name + beer.tagline)
            .toLowerCase()
            .includes("lager"),
      },
      {
        label: "Stout",
        isActive: false,
        callbackFn: (beer: IBeer) =>
          (beer.description + beer.name + beer.tagline)
            .toLowerCase()
            .includes("stout"),
      },
      {
        label: "Porter",
        isActive: false,
        callbackFn: (beer: IBeer) =>
          (beer.description + beer.name + beer.tagline)
            .toLowerCase()
            .includes("porter"),
      },
      {
        label: "Blonde Ale",
        isActive: false,
        callbackFn: (beer: IBeer) =>
          (beer.description + beer.name + beer.tagline)
            .toLowerCase()
            .includes("blonde"),
      },
      {
        label: "Brown Ales",
        isActive: false,
        callbackFn: (beer: IBeer) =>
          (beer.description + beer.name + beer.tagline)
            .toLowerCase()
            .includes("brown"),
      },
      {
        label: "Pale Ale",
        isActive: false,
        callbackFn: (beer: IBeer) =>
          (beer.description + beer.name + beer.tagline)
            .toLowerCase()
            .includes("pale"),
      },
      {
        label: "Pilsener",
        isActive: false,
        callbackFn: (beer: IBeer) =>
          (beer.description + beer.name + beer.tagline)
            .toLowerCase()
            .includes("pilsener"),
      },
      {
        label: "Wheat",
        isActive: false,
        callbackFn: (beer: IBeer) =>
          (beer.description + beer.name + beer.tagline)
            .toLowerCase()
            .includes("wheat"),
      },
    ],
  },
];

export default beerFilters;
