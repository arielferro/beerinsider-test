import IBeer from "../interfaces/IBeer";
import { IFilter, IFilterGroup } from "../interfaces/IFilter";
import IPagination from "../interfaces/IPagination";

export const findBeerById = (
  beers: IBeer[] = [],
  id: string = ""
): IBeer | null => {
  return beers.find((beer: IBeer) => beer.id.toString() === id) || null;
};

const filterBeersBySearchTerm = (
  beers: IBeer[] = [],
  searchTerm: string = ""
): IBeer[] | null => {
  return beers.filter((beer: IBeer) =>
    new RegExp(searchTerm, "i").test(
      `${beer.name} ${beer.tagline} ${beer.description} `
    )
  );
};

export const filterBeers = (
  beers: IBeer[],
  filtersGroups: IFilterGroup[],
  searchTerm: string = ""
): IBeer[] | null => {
  let appliedFilters: IFilter[] = [];
  filtersGroups.forEach((group: IFilterGroup) => {
    appliedFilters = appliedFilters.concat(
      group.filters.filter((f) => f.isActive)
    );
  });
  const beerSlice = beers.filter((beer: IBeer) =>
    appliedFilters.every((f) => f.callbackFn(beer))
  );
  return filterBeersBySearchTerm(beerSlice, searchTerm);
};

export const getSummary = (
  text: string,
  maxChars: number = 200,
  ending: string = "(...)"
) => {
  return text.length < maxChars
    ? text
    : `${text
        .substring(0, maxChars)
        .split(" ")
        .slice(0, -1)
        .join(" ")} ${ending}`;
};

export const getBeersPaginationParameters = (
  numberOfItems: number = 0,
  currentPage: number = 1,
  setCurrentPage: Function
): IPagination => {
  const itemsPerPage: number = 20;
  const numberOfPages: number = Math.floor(numberOfItems / itemsPerPage);
  const fromIndex: number =
    currentPage === 1 ? 0 : currentPage * itemsPerPage - itemsPerPage;
  const toIndex: number =
    fromIndex + itemsPerPage > numberOfItems
      ? numberOfItems
      : fromIndex + itemsPerPage;
  return {
    itemsPerPage,
    numberOfItems,
    numberOfPages,
    fromIndex,
    toIndex,
    currentPage,
  } as IPagination;
};

export const getBeersPaginationIndexes = (
  numberOfPages: number,
  currentPage: number,
  pagesCutOff: number = 5
) => {
  let start;
  let end;
  const ceiling = Math.ceil(pagesCutOff / 2);
  const floor = Math.floor(pagesCutOff / 2);

  if (numberOfPages < pagesCutOff) {
    start = 0;
    end = numberOfPages;
  } else if (currentPage >= 1 && currentPage <= ceiling) {
    start = 0;
    end = pagesCutOff;
  } else if (currentPage + floor >= numberOfPages) {
    start = numberOfPages - pagesCutOff;
    end = numberOfPages;
  } else {
    start = currentPage - ceiling;
    end = currentPage + floor;
  }
  return {
    last: currentPage < numberOfPages - Math.floor(pagesCutOff / 2),
    first: currentPage > Math.floor(pagesCutOff / 2) + 1,
    indexes: Array(numberOfPages).fill(1, start, end),
  };
};
