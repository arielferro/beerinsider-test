import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import Header from "./components/Header";
import Catalog from "./components/Catalog";
import { fetchBeers } from "./redux";
import Detail from "./components/Detail";
import Footer from "./components/Footer";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);
  useEffect(() => {
    setTimeout(() => {
      dispatch(fetchBeers(page, setPage));
    }, 1000);
  }, [dispatch, page]);

  return (
    <div className="App">
      <Router>
        <Header />
        <main>
          <Switch>
            <Route path="/beer/:id">
              <Detail />
            </Route>
            <Route path="/">
              <Catalog />
            </Route>
          </Switch>
        </main>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
