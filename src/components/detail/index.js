import React from "react";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import MESSAGES from '../../config/constants'
import { beersSelector } from "../../redux/beers/selectors"
import { findBeerById } from "../../functions";
import LoadingBox from "../LoadingBox";
import ErrorBox from "../ErrorBox";
import "./index.css";

const Detail = () => {
  const { beers, hasError, isLoading } = useSelector((state) => beersSelector(state));
  const { id } = useParams();

  if (isLoading) return <LoadingBox />;
  if (hasError) return <ErrorBox />;

  const beer = findBeerById(beers, id);

  if (!beer) return <ErrorBox message={MESSAGES.NO_BEER_FOUND} />;

  return (
    <div className="beer-detail">
      <h2>{beer.name}</h2>
      <h3>{beer.tagline}</h3>
      <img src={beer.image_url} alt={beer.name} />
      <hr />
      <h5>{MESSAGES.beer_details}</h5>
      <p>{beer.description}</p>
    </div>
  );
};

export default Detail;
