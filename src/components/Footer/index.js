import React from "react";
import "./index.css";

const Footer = () => {
  return (
    <footer>
      <nav>
        <a href="https://www.facebook.com">
          <i className="fa fa-facebook-square fa-3x social social-fb"></i>
        </a>
        <a href="https://twitter.com">
          <i className="fa fa-twitter-square fa-3x social social-tw"></i>
        </a>
        <a href="https://plus.google.com">
          <i className="fa fa-google-plus-square fa-3x social social-gp"></i>
        </a>
        <a href="mailto:aferro@sparkdigital.com">
          <i className="fa fa-linkedin-square fa-3x social social-in"></i>
        </a>
      </nav>
      Copyright &copy; Spark Digital 2021
    </footer>
  );
};

export default Footer;
