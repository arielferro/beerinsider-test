import styled from "styled-components";

export const SectionCenter = styled.section`
  display: flex;
  justify-content: center;
`;
