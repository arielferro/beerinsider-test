import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  beersSelector,
  filteredBeersSelector,
} from "../../redux/beers/selectors";
import useQuery from "../../hooks/useQuery";
import { getBeersPaginationParameters } from "../../functions";
import { setSearchTerm } from "../../redux/beers/actions";
import MESSAGES from "../../config/constants";
import Beer from "./Beer";
import LoadingBox from "../LoadingBox";
import PaginationBar from "./PaginationBar";
import "./index.css";

const Catalog = () => {
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => beersSelector(state));
  const foundBeers = useSelector((state) => filteredBeersSelector(state));

  const [currentPage, setCurrentPage] = useState(1);
  const { page, search } = useQuery(["page", "search"]);

  useEffect(() => {
    if (page) setCurrentPage(Number(page));
    if (typeof search === "string") {
      dispatch(setSearchTerm(search));
    }
  }, [dispatch, search, page]);

  const paginationParameters = getBeersPaginationParameters(
    foundBeers.length,
    currentPage,
    setCurrentPage
  );

  const foundBeersSlice = foundBeers.slice(
    paginationParameters.fromIndex,
    paginationParameters.toIndex
  );
  const noBeersFound = isLoading === false && foundBeersSlice.length === 0;

  return (
    <>
      <div className="beers-list-header">{isLoading && <LoadingBox />}</div>
      {noBeersFound ? (
        <div className="beers-list-empty">{MESSAGES.NO_BEERS_FOUND}</div>
      ) : (
        <>
          <div className="grid-container">
            {foundBeersSlice.map?.((beer) => (
              <Beer {...beer} key={beer.id} />
            ))}
          </div>
          <PaginationBar {...paginationParameters} />
        </>
      )}
    </>
  );
};

export default Catalog;
