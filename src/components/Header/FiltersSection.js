import React from "react";

const FiltersSection = (props) => {



    const { label, multiple, filters, index, onChange } = props;

    return (
        <fieldset>
            <h4>{label}</h4>
            {filters.map((filter, ix) => {
                return (
                    <label key={ix}>
                        <input
                            type={multiple ? "checkbox" : "radio"}
                            checked={filter.isActive}
                            onChange={(e) => onChange(index, ix)}
                        />
                        {filter.label}
                    </label>
                );
            })}
        </fieldset>
    );
};

export default FiltersSection;
