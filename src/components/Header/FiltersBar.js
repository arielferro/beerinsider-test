import React from "react";
import { useDispatch, useSelector } from "react-redux";
import MESSAGES from "../../config/constants";
import beerFilters from "../../config/filters";
import { setFilters } from "../../redux";
import { beersSelector } from "../../redux/beers/selectors";
import FiltersSection from "./FiltersSection";
import * as S from "../styled";

const FiltersBar = ({ onClose }) => {
  const dispatch = useDispatch();
  const { filters } = useSelector((state) => beersSelector(state));

  const updateFilters = (filtersGroupIndex, filterIndex) =>
    dispatch(setFilters(filters, filtersGroupIndex, filterIndex));
  const resetFilters = () => {
    dispatch(setFilters([...beerFilters]));
    onClose();
  };
  return (
    <div className="beers-filters">
      <h3>{MESSAGES.FILTER_BEERS_BY}</h3>
      <form>
        {filters.map((filtersGroup, index) => {
          const props = {
            ...filtersGroup,
            key: index,
            onChange: updateFilters,
            index,
          };
          return <FiltersSection {...props} />;
        })}
      </form>
      <S.SectionCenter>
        <button type="button" className="btn" onClick={onClose}>
          {MESSAGES.CLOSE}
        </button>
        &nbsp;
        <button type="button" className="btn" onClick={resetFilters}>
          {MESSAGES.RESET_FILTERS}
        </button>
      </S.SectionCenter>
    </div>
  );
};

export default FiltersBar;
