import React from "react";
import propTypes from "prop-types";
import MESSAGES from "../config/constants";
import "./ErrorBox.css";

const ErrorBox = ({ message }) => {
  return <div className="message-box message-box-error">{message}</div>;
};

ErrorBox.defaultProps = {
  message: MESSAGES.GENERIC_ERROR,
};

ErrorBox.propTypes = {
  message: propTypes.string,
};

export default ErrorBox;
