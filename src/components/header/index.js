import React from "react";
import { Link } from "react-router-dom";
import SearchBar from "./SearchBar";
import MESSAGES from '../../config/constants'
import "./index.css";

const Header = () => {
  return (
    <header>
      <h1>{MESSAGES.BEER_INSIDER}</h1>

      <Link to="/" aria-label={MESSAGES.CATALOG}>
        <svg viewBox="13 2 20 20">
          <title>{MESSAGES.CATALOG}</title>
          <fill d="m0,0l24,0l0,24l-24,0l0,-24zm18.31,30l-2.76,5l2.76,-5z" />
          <path
            id="svg_2"
            d="m16,4zm-9,14c-1.1,0 -1.99,0.9 -1.99,2s0.89,2 1.99,2s2,-0.9 2,-2s-0.9,-2 -2,-2zm10,0c-1.1,0 -1.99,0.9 -1.99,2s0.89,2 1.99,2s2,-0.9 2,-2s-0.9,-2 -2,-2zm-9.83,-3.25l0.03,-0.12l0.9,-1.63l7.45,0c0.75,0 1.41,-0.41 1.75,-1.03l3.86,-7.01l-1.74,-0.96l-0.01,0l-1.1,2l-2.76,5l-7.02,0l-0.13,-0.27l-2.24,-4.73l-0.95,-2l-0.94,-2l-3.27,0l0,2l2,0l3.6,7.59l-1.35,2.45c-0.16,0.28 -0.25,0.61 -0.25,0.96c0,1.1 0.9,2 2,2l12,0l0,-2l-11.58,0c-0.13,0 -0.25,-0.11 -0.25,-0.25z"
          />
          <text x="25" y="15">
            {MESSAGES.CATALOG}
          </text>
        </svg>
      </Link>

      <SearchBar />
    </header>
  );
};

export default Header;
