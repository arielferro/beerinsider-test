interface IPagination {    
        itemsPerPage: number;
        numberOfItems: number;  
    numberOfPages: number;
    fromIndex: number;
    toIndex: number;
}

export default IPagination;