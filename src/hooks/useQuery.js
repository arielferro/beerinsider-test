import { useLocation } from "react-router-dom";

const useQuery = (paramNames) => {
  const params = {};
  const searchParams = new URLSearchParams(useLocation().search);
  paramNames.map(
    (param) => (params[param] = searchParams.get(param))
  );

  return params;
};

export default useQuery;
